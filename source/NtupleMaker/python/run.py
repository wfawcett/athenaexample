import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "-f",
    "--filesInput",
    help="The input files to run over"
    )
parser.add_argument(
    "-o", "--output", default="myfile.root", help="The output ROOT File"
)
parser.add_argument(
    "-e",
    "--evtMax",
    type=int,
    default=-1,
    help="The number of events to run over, -1 means all",
)
parser.add_argument(
    "-l",
    "--log-level",
    type=str.upper,
    default="INFO",
    choices=("VERBOSE", "DEBUG", "INFO", "WARNING", "ERROR"),
    help="The message level to set",
)
args = parser.parse_args()
print(f"Input: {args.filesInput}, output: {args.output}")

from AthenaConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaCommon import Constants
from AthenaRootComps.xAODEventSelectorConfig import xAODReadCfg

# Setup the input
ConfigFlags.Input.Files = [args.filesInput] # N.B turn this into a list as we expect a single file
ConfigFlags.Exec.OutputLevel = getattr(Constants, args.log_level)
ConfigFlags.Exec.MaxEvents = args.evtMax
ConfigFlags.lock()

acc = MainServicesCfg(ConfigFlags)
acc.merge(xAODReadCfg(ConfigFlags))
# Suppress the printing on every event
acc.getService(acc.getAppProps()["EventLoop"]).EventPrintoutInterval = 1000
# Add our output stream to the hist svc
acc.addService(
    CompFactory.THistSvc(Output=[f"MYSTREAM DATAFILE='{args.output}' OPT='RECREATE'"])
)
# Add our algorithm
acc.addEventAlgo(
    CompFactory.NtupleMakerAlg(
        "NtupleMakerAlg", ascii_file="NtupleMaker/cow.txt", RootStreamName="MYSTREAM"
    )
)
# Run the job
acc.run()
